# Exercice 7.1 #

### Une application web avec Spring Boot ###

Sources de l'application web.

#### Tags ####

* Q1 : Initialisation du projet avec Spring Initializr
* Q2 : Retour d'un simple message
* Q3 : Poossibilité d'exécution avec Docker
* Q4txt / Q5txt : Reponse aux question 4 et 5, chaines de caractères retournées
* Q4Q5JSON : Reponse aux question 4 et 5, documents retournés au format JSON
* Qbonus1 / Qbonus2 : Questions traitées séparément
* FINAL : Synthese (résolution des conflits) pour avoir l'ensemble des fonctionnalités développées

#### Branches ####

Développement suivant le modèle feature branch.

Tous les tags exceptés Q1 (Premier commit sur master) et FINAL (derniere fusion sur master) correspondent à une branche distincte.

Elles sont visibles (graphiquement) sur l'historique des commits.

On a supprimé automatiquement les branches après les merge jusqu'à pratiquement la fin du projet, où l'on s'est dit que vous préfèreriez surement qu'on les conserve. C'est pour cela que 2 branches uniquement sont consultables.

### Compilation ###

Sur les machines virtuelles de l'UVSQ (notre outil de travail), l'utilisateur n'a pas tous les droits d'accès pour manipuler le projet. Les commandes sont donc légèrement différentes de celles énoncées dans le mail : 

* sudo mvn clean : Suppression des fichiers générés
* sudo mvn package : Compilation du projet
* sudo mvn docker:build : Création de  l'image avec docker

### Exécution ###

* java -jar target/spring-todo-0.0.1SNAPSHOT.jar : Lancement avec java
* sudo docker run -p 8080:8080 -t root/spring-todo : Lancement avec Docker

:warning: Lorsque l'on ferme l'appli avec docker, le port ne se ferme et l'on ne peut pas la réexécuter derrière : 

* sudo docker ps : pour lister les conteneurs qui fonctionnent
* sudo docker kill <ID_docker> : terminer le conteneur n°ID_docker

### Utilisation ###

Une fois que l'application tourne sur un terminal, on peut la tester.

* Avec les commandes curl en ouvrant un autre terminal

    * curl localhost:8080 : requete HTTP:GET
    * curl --data "title=NomTache" localhost:8080 : requete HTTP:POST

* Sur un navigateur ( URL : http://localhost:8080 )

Résultats attendus : 

* Q2 : GET uniquement, un simple message
* Q3 : GET uniquement, idem seulement, l'appli tourne grâce à Docker
* Q4 : GET uniquement, une liste vide (la commande pour ajouter des taches n'est pas encore programmée
* Q5 : GET, la liste de taches // POST : retourne la tache ajoutée (et l'ajoute)
* Q**1 : idem, mais le traitement se fait grâce à une base de données relationnelle
* Q**2 / FINAL : documents HTML, il faut ouvrir le navigateur web, on manipule grâce à l'interface web

### Auteurs ###

* Sonny Klotz
* Florian Lienhardt