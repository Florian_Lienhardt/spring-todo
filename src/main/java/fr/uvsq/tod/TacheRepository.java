package fr.uvsq.tod;

import org.springframework.data.repository.CrudRepository;

public interface TacheRepository extends CrudRepository<Tache, String> {
}
