package fr.uvsq.tod;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Tache {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	private String creator;
	private String description;

	
	protected Tache(){}
	
	public long getId() {return id;}
	public void setId(long id) {this.id = id;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public String getCreator() {return creator;}
	public void setCreator(String creator) {this.creator = creator;}
	
	public String getDescription() {return description;}
	public void setDescription(String description) {this.description = description;}
}
