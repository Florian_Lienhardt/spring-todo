package fr.uvsq.tod;

import java.util.ArrayList;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class ListeTachesController {
	
	@Autowired
	TacheRepository repository;

    @PostMapping("/")
    public String ajoutTache(@ModelAttribute Tache tache, Model model) {
		repository.save(tache);
		return "redirect:/";
    }
    
    @GetMapping("/")
	public String liste(@ModelAttribute Tache tache, Model model) {
		model.addAttribute("taches", repository.findAll());
		model.addAttribute("tache", new Tache());
        return "listeTaches";
	}
}



